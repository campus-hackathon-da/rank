package io.swaghub.rank

import com.mongodb.BasicDBObject
import com.mongodb.BasicDBObjectBuilder
import com.mongodb.DBObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

@Service
class StatisticsService @Autowired constructor(
        val mongoTemplate: MongoTemplate
) {
    fun getEventStatistics(repositoryName: String): Iterable<DBObject> {
        val events = mongoTemplate.db.getCollection("events")

        val matchRepositoryName = BasicDBObject("\$match",
                BasicDBObject("repo.name", repositoryName)
        )

        val concat = BasicDBObject("\$concat", listOf(
                "\$type",
                BasicDBObject("\$ifNull", listOf(
                        "\$payload.action",
                        ""
                ))
        ))

        val projectionFields = BasicDBObject()
        projectionFields.put("name", "\$repo.name")
        projectionFields.put("created_at", "\$created_at")
        projectionFields.put("type", concat)

        val projectNecessaryFields = BasicDBObject("\$project", projectionFields)

        val firstGroupId = BasicDBObject()
        firstGroupId.put("type", "\$type")
        firstGroupId.put("interval", BasicDBObject(
                "\$trunc",
                BasicDBObject(
                        "\$divide",
                        listOf(
                                BasicDBObject(
                                        "\$subtract",
                                        listOf(
                                                "\$created_at",
                                                Date(2014 - 1900, 0, 1)
                                        )
                                ),
                                1 * 24 * 60 * 60 * 1000
                        )
                )
        ))

        val firstGroup = BasicDBObject()
        firstGroup.put("_id", firstGroupId)
        firstGroup.put("count", BasicDBObject("\$sum", 1))

        val groupByTypeAndInterval = BasicDBObject("\$group", firstGroup)

        val groupByIntervalAndCount = BasicDBObject("\$group",
                BasicDBObjectBuilder()
                        .add("_id", BasicDBObject("interval", "\$_id.interval"))
                        .add("counts", BasicDBObject("\$addToSet",
                                BasicDBObjectBuilder()
                                        .add("type", "\$_id.type")
                                        .add("count", "\$count")
                                        .get()
                        ))
                        .get()
        )

        val res = events.aggregate(
                matchRepositoryName,
                projectNecessaryFields,
                groupByTypeAndInterval,
                groupByIntervalAndCount
        )

        return res.results()
    }
}
