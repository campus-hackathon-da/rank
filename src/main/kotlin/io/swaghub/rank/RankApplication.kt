package io.swaghub.rank

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import javax.annotation.PostConstruct

@SpringBootApplication
class RankApplication

fun main(args: Array<String>) {
    val context = SpringApplication.run(RankApplication::class.java, *args)
}
